package main

import (
	"encoding/json"
	"io"
	"log"
	"net"
)

type IPCHandler struct {
	Observers ObserverMap
	Listeners ListenerMap

	events    chan IPCEvent
	requests  chan chan *IPCResponse
	responses chan IPCResponse
	requestId int
	sock      net.Conn
}

type ObserverMap map[int]chan IPCEvent

type ListenerMap map[string]chan IPCEvent

type _IPCData struct {
	Data interface{} `json:"data,omitempty"`

	*IPCEvent
	*IPCResponse
}

// IPCEvent represents an mpv event - start-file, end-file, etc
type IPCEvent struct {
	Event string      `json:"event"`
	Id    int         `json:"id,omitempty"`
	Name  string      `json:"name,omitempty"`
	Data  interface{} `json:"data,omitempty"`
}

// IPCResponse represents a response to an IPCCommand
type IPCResponse struct {
	Error     string      `json:"error"`
	Data      interface{} `json:"data,omitempty"`
	RequestID *int        `json:"request_id,omitempty"`
}

// IPCCommand represents a request to perform an action in mpv
type IPCCommand struct {
	Command   []interface{} `json:"command"`
	RequestID int           `json:"request_id,omitempty"`
}

func NewIPCHandler() (*IPCHandler, error) {
	handler := IPCHandler{
		Observers: make(map[int]chan IPCEvent),
		Listeners: make(map[string]chan IPCEvent),

		events:    make(chan IPCEvent),
		requests:  make(chan chan *IPCResponse),
		responses: make(chan IPCResponse),
		requestId: 0,
		sock:      nil,
	}

	handler.Connect(configPlayerSocketPath)

	go handler.reader()
	go handler.eventHandler()
	go handler.responseHandler()

	return &handler, nil
}

func (m ObserverMap) Register(eventId int, ch chan IPCEvent) error {
	if existing, exists := m[eventId]; exists {
		close(existing)
	}
	m[eventId] = ch
	return nil
}

func (m ListenerMap) Register(event string, ch chan IPCEvent) error {
	if existing, exists := m[event]; exists {
		close(existing)
	}
	m[event] = ch
	return nil
}

func (h *IPCHandler) Connect(path string) error {
	conn, err := net.Dial("unix", path)
	if err != nil {
		return err
	}

	h.sock = conn
	return nil
}

func (h *IPCHandler) Call(args ...interface{}) (*IPCResponse, error) {
	h.requestId += 1
	log.Println(h.requestId)
	resp, err := h.CallIPCCommand(IPCCommand{args, h.requestId})
	return resp, err
}

// CallIPCCommand marshals cmd as JSON and sends it to the configured mpv control socket.
func (h IPCHandler) CallIPCCommand(cmd IPCCommand) (*IPCResponse, error) {
	response := make(chan *IPCResponse)
	h.requests <- response

	data, err := json.Marshal(cmd)
	if err != nil {
		return nil, err
	}

	if _, err := h.sock.Write(data); err != nil {
		return nil, err
	}

	if _, err := h.sock.Write([]byte{'\n'}); err != nil {
		return nil, err
	}

	return <-response, nil
}

func (h *IPCHandler) eventHandler() {
	log.Println("listening for events")

	for event := range h.events {
		if evchan, registered := h.Listeners[event.Event]; registered {
			evchan <- event
			continue
		}
		log.Printf("unregistered mpv event: %s\n", event.Event)
	}
}

func (h *IPCHandler) responseHandler() {
	log.Println("listening for responses")
	for request := range h.requests {
		response := <-h.responses
		if response.RequestID != nil {
			log.Printf(
				"mpv response (id %d): '%s'; %v\n",
				*response.RequestID, response.Error, response.Data,
			)
			request <- &response
		} else {
			log.Printf(
				"mpv response: '%s', %v\n",
				response.Error, response.Data,
			)
			request <- nil
		}
	}
}

// reader unmarshals data coming from the mpv IPC and writes it to the
// IPCHandler's channels, events and responses.
func (h IPCHandler) reader() error {
	dec := json.NewDecoder(h.sock)

	for dec.More() {
		var data _IPCData
		if err := dec.Decode(&data); err == io.EOF {
			break
		} else if err != nil {
			log.Panicln("error reading JSON response from socket:", err)
		}

		if data.IPCEvent != nil {
			data.IPCEvent.Data = data.Data
			h.events <- *data.IPCEvent
		}
		if data.IPCResponse != nil {
			data.IPCResponse.Data = data.Data
			h.responses <- *data.IPCResponse
		}
	}

	// TODO: maybe trigger some kind of recovery callback. all listeners
	// and observers need to be reregistered if the stream breaks and has
	// to be reopened. that's why this is a panic
	log.Panicln("reader: stream broken!")

	return nil
}
